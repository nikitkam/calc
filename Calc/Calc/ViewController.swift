//
//  ViewController.swift
//  Calc
//
//  Created by Mikita Marakhouski on 07.04.16.
//  Copyright © 2016 Mikita Marakhouski. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var display: UILabel!
    
    @IBOutlet weak var LogDisplay: UILabel!
      
    
    
    
    var DisplayValue: Double {
     
        get {
          return NSNumberFormatter().numberFromString(display.text!)!.doubleValue
        }
        
        set {
            display.text = "\(newValue)"
            MiddleTyping = false
        }
    }
    
    var MiddleTyping: Bool = false
 
    
    @IBAction func AppendDigit(sender: UIButton) {
        
        let digit = sender.currentTitle!
        
        if MiddleTyping {
            display.text=display.text!+digit
        }
        else {
  
            display.text = digit
            MiddleTyping = true
        }
        
       
        }
    
    func Logger(LogerValue: String)  {
        LogDisplay.text = LogDisplay.text! + LogerValue + " "
    }
    
    @IBAction func Cleaner() {
        display.text = "0"
        LogDisplay.text = " "
        OperandStack = Array<Double>()
    }
    
    @IBAction func DOT() {
        if display.text!.rangeOfString(".") == nil {
            display.text = display.text! + "."
            MiddleTyping = true
        }
    }
    
    
    @IBAction func MinusPlus() {
        if MiddleTyping {
        DisplayValue = -1 * DisplayValue
        }
        else {
            display.text = "-"
            MiddleTyping = true
        }
        
    }
    
    @IBAction func operate(sender: UIButton) {
        let OpType = sender.currentTitle!
        Logger(OpType)
        if MiddleTyping {
            Return()
        }
        
        switch OpType {
            case "+": peformOperation{ $0 + $1 }
            case "−": peformOperation{ $1 - $0 }
            case "÷": peformOperation{ $1/$0 }
            case "×": peformOperation{ $0*$1 }
            case "√": perfomUnaryOperation{ sqrt($0)}
            case "cos": perfomUnaryOperation{ sin($0)}
            case "sin": perfomUnaryOperation{ cos($0)}
            case "π": peformConstant{M_PI}

            
        default:
            break
        }
      
    }
    
    
    
    var OperandStack = Array <Double>()
    
    @IBAction func Return() {
        OperandStack.append(DisplayValue)
        Logger(display.text!)
        MiddleTyping = false
        print("\(OperandStack)")
    }
    
    
 
    func peformOperation ( operation:(Double,Double) -> Double ){
        if OperandStack.count >= 2 {
            Logger("= ")
            DisplayValue = operation(OperandStack.removeLast(),OperandStack.removeLast())
            Return()
        }
    }
    
    func perfomUnaryOperation(UnaryOperation:Double->Double){
        if OperandStack.count >= 1 {
            Logger("= ")
            DisplayValue = UnaryOperation(OperandStack.removeLast())
            Return()
            
        }
    }
    

    func peformConstant(Constant:() -> Double){
        Logger("= ")
        DisplayValue = Constant()
        Return()
        
    }
    
}

